import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider    } from 'react-redux';
import { createStore } from 'redux';

//import { rootReducer  } from './reducers/rootReducer'; 
//import { initialState } from './reducers/info'; 

const initialState = {
	target:  parseInt(Math.floor( Math.random() * ( (-200) - 200) + 200 ) ),
	counter: 0,
}



const reducer = (state = initialState, action) => {
	switch(action.type){
		case 'DECREMENT_6':
			return Object.assign(
				{},
				state,
				{counter: state.counter - 6}
			)
		case 'DECREMENT_15':
			return Object.assign(
				{},
				state,
				{counter: state.counter - 15}
			)
		case "INCREMENT_28":
			return Object.assign(
				{},
				state,
				{counter: state.counter + 28}
			)
		case "INCREMENT_7":
			return Object.assign(
				{},
				state,
				{counter: state.counter + 7}
			)
		case "MULTIPLY":
			return Object.assign( 
				{},
				state,
				{counter: state.counter * 2}
			)
		default:
			return state;
	}
}

const store = createStore(reducer, initialState)

ReactDOM.render(
	<Provider store = {store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
