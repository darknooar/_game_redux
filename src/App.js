import React, { Component } from 'react';
import './App.css';

import { connect } from 'react-redux';

class App extends Component {
	
	render() {
		return (
			<div className="App">
				<p> Target: {this.props.target} </p>
				<p>	Count: {this.props.count}   </p>
				<button onClick={this.props.dec_16_click} 	className="main_btn" >  -15   </button>
				<button onClick={this.props.dec_6_click} 	className="main_btn" >  -6  </button>
				<button onClick={this.props.inc_7_click} 	className="main_btn" >  +7   </button>
				<button onClick={this.props.inc_28_click} 	className="main_btn" >  +28  </button><br />
				<button onClick={this.props.mult_Click} 	className="main_btn" >  x2   </button>

			</div>
		);
	}
}

const mapStateToProps = state => ({
	target: state.target,
	count: state.counter,
})

const mapDispatchToProps = dispatch => ({
	dec_16_click: () => dispatch ({ type: 'DECREMENT_15' }),
	dec_6_click:  () => dispatch ({ type: 'DECREMENT_6'  }),
	inc_7_click:  () => dispatch ({ type: 'INCREMENT_7'  }),
	inc_28_click: () => dispatch ({ type: 'INCREMENT_28' }),
	mult_Click:   () => dispatch ({ type: 'MULTIPLY' 	 }),
})

export default connect(mapStateToProps, mapDispatchToProps) (App);